<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Belajar extends CI_Controller {

	function __construct(){
		parent::__construct();
      $this->load->library('form_validation');
      $this->load->model('m_form');
	}

	public function index(){
    $this->load->view('page_header');
		$this->load->view('v_home');
	}

	public function form(){
		$this->load->view('page_header');
		$this->load->view('v_form');
	}

  public function input(){
		$this->form_validation->set_rules('sector', 'Sector', 'required');
    if ($this->form_validation->run() == FALSE) {
      $this->load->view('v_form');
    } else {
      $this->m_form->inputform();
			$this->load->view('page_header');
			$this->load->view('v_form');
    }
  }
	public function list(){
		$this->load->view('page_header');
		$data_list = $this->m_form->tampilform();
		$this->load->view('v_list',['data'=>$data_list]);
	}

	public function hapus($id){
		$data_list = $this->m_form->deleteform($id);
		redirect('belajar/list');
	}
 	public function about(){
 		$this->load->view('page_header');
 		$this->load->view('v_tentang');
 	}
}
