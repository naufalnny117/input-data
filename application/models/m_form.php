<?php
defined('BASEPATH') OR exit('No direct script access allowed');



class m_form extends CI_Model{
  public function inputform(){

    $data = [
				'tgl' => $this->input->post('tgl_control',true),
				'jam' => $this->input->post('jam_control',true),
				'sector' => $this->input->post('sector',true),
				'kategori' => $this->input->post('kategori',true),
        'mA_n_A' => $this->input->post('mA',true),
        'fwd' => $this->input->post('fwd',true),
        'rfl' => $this->input->post('rfl',true),
        'ket' => $this->input->post('keter',true)
			];

      $this->db->insert('pemancar',$data);
  }

  public function tampilform(){
      $query = $this->db->get('pemancar');
      return $query->result();
  }

  public function deleteform($id){
      $this->db->delete('pemancar', array('id' => $id));
  }
}
