<!DOCTYPE html>
  <html lang="" dir="ltr">
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>">
      <script src="<?php echo base_url('assets/js/jquery.min.js') ?>"></script>
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery.dataTables.css') ?>">
      <script type="text/javascript" charset="utf8" src="<?php echo base_url('assets/js/jquery.dataTables.js') ?>"></script>
      <script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
      <script src="<?php echo base_url('assets/js/all.min.js') ?>"></script>
      <link rel="stylesheet" href="<?php echo base_url('assets/css/all.min.css') ?>" >
      <title></title>
      <style>
        .bodiadm{
          margin-top: 90px;
        }
        .button_tambah{
    			background-color: #FFFFFF;
    			color: #ec4638;
    			border: 2px solid #ec4638;
    			padding: 5px 10px;
    			margin: 0px 0px 4px 0px;
    			font-size: 15px;
    			border-radius: 25px;
    		}
    		.button_tambah:hover{
    			background-color: #ec4638;
    			color: #FFFFFF;
    		}
      </style>
    </head>
    <body class="bodiadm">
      <div class="container">
        <div class="box">
          <center><h3>Rekap Data Pemancar</h3></center>
          <br>
          <table class="table table-bordered" id="table">
            <thead>
              <tr>
                <th>No</th>
                <th>Hari</th>
                <th>Tanggal Control</th>
                <th>Jam Control</th>
                <th>Sector Radio</th>
                <th>Kategori Radio</th>
                <th>mA and A</th>
                <th>FWD</th>
                <th>RFL</th>
                <th>Keterangan</th>
                <th>Edit</th>
                <th>Hapus</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($data as $d) { ?>
              <tr>
                <form>
                  <td><?php echo $d->no; ?></td>
                  <td><?php
                          setlocale(LC_ALL, 'id-ID', 'id_ID');
                          echo strftime("%A", strtotime($d->tgl));

                      ?>
                  </td>
                  <td><?php
                          echo strftime("%d %B %Y", strtotime($d->tgl));
                      ?>
                  </td>
                  <td><?php echo $d->jam ?></td>
                  <td><?php echo $d->sector ?></td>
                  <td><?php echo $d->kategori ?></td>
                  <td><?php echo $d->mA_n_A ?></td>
                  <td><?php echo $d->fwd ?></td>
                  <td><?php echo $d->rfl ?></td>
                  <td><?php echo $d->ket ?></td>

                  <td><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#edit "><i class="fas fa-user-edit"></i></button></td>
                  <td><a href="" type="button" class="btn btn-danger" onClick="return confirm('Apakah Anda Yakin?')" ><i class="fas fa-user-times"></i></a></td>
                </form>
                </tr>
                  <?php } ?>
              </tbody>

          </table>

        </div>
      </div>
    </body>
  </html>
