<!DOCTYPE html>
<html>
<head>
	<style>
		.bodi{
			margin-top: 90px;
			margin-left: 20px;
		}
		.tentang{
			margin-bottom: 40px;
		}
	</style>
	<title></title>
</head>
<body>
	<div class="bodi">
		<h2 class="tentang" style="text-align: left;">Tentang RRI</h2>
		<p>RRI adalah satu-satunya radio yang menyandang nama negara yang siarannya ditujukan untuk kepentingan bangsa dan negara, RRI sebagai Lembaga Penyiaran Publik yang independen, netral dan tidak komersial yang berfungsi memberikan pelayanan siaran informasi, pendidikan, hiburan yang sehat, kontrol sosial, serta menjaga citra positif bangsa di dunia internasional <br><br> Besarnya tugas dan fungsi RRI yang diberikan oleh negara melalui UU no 32 tahun 2002 tentang penyiaran, PP 11 tahun 2005 tentang Lembaga Penyiaran Publik, serta PP 12 Tahun 2005, RRI dikukuhkan sebagai satu-satunya lembaga penyiaran yang dapat berjaringan secara nasional dan dapat bekerja sama dalam siaran dengan lembaga penyiaran Asing. <br><br>
		Dengan kekuatan 62 stasiun penyiaran termasuk Siaran Luar Negeri dan 5 (lima) satuan kerja (satker) lainnya yaitu Pusat Pemberitaan, Pusat Penelitian dan Pengembangan (Puslitbangdiklat) Satuan Pengawasan Intern, serta diperkuat 16 studio produksi serta 11 perwakilan RRI di Luar negri RRI memiliki 61 (enampuluh satu) programa 1, 61 programa 2, 61 programa 3, 14 programa 4 dan 7 studio produksi maka RRI setara dengan 205 stasiun radio. <br><br>
		Dengan kekuatan 62 stasiun penyiaran termasuk Siaran Luar Negeri dan 5 (lima) satuan kerja (satker) lainnya yaitu Pusat Pemberitaan, Pusat Penelitian dan Pengembangan (Puslitbangdiklat) Satuan Pengawasan Intern, serta diperkuat 16 studio produksi serta 11 perwakilan RRI di Luar negri RRI memiliki 61 (enampuluh satu) programa 1, 61 programa 2, 61 programa 3, 14 programa 4 dan 7 studio produksi maka RRI setara dengan 205 stasiun radio. <br><br>
		VISI LPP RRI: <br>
		Menjadikan LPP RRI radio berjaringan terluas, pembangun karakter bangsa, berkelas dunia. <br><br>
		MISI LPP RRI <br>
		1.	Memberikan pelayanan informasi terpercaya yang dapat menjadi acuan dan sarana kontrol sosial masyarakat dengan memperhatikan kode etik jurnalistik/kode etik penyiaran <br>
		2.	Mengembangkan siaran pendidikan untuk mencerahkan, mencerdaskan, dan memberdayakan serta mendorong kreativitas masyarakat dalam kerangka membangun karakter bangsa. <br>
		3.	Menyelenggarakan siaran yang bertujuan menggali, melestarikan dan mengembangkan budaya bangsa, memberikan hiburan yang sehat bagi keluarga, membentuk budi pekerti dan jati diri bangsa di tengah arus globalisasi. <br>
		4.	Menyelenggarakan program siaran berperspektif gender yang sesuai dengan budaya bangsa dan melayani kebutuhan kelompok minoritas. <br>
		5.	Memperkuat program siaran di wilayah perbatasan untuk menjaga kedaulatan NKRI. <br>
		6.	Meningkatkan kualitas siaran luar negeri dan program siaran yang mencerminkan politik negara dan citra positif bangsa. <br>
		7.	Meningkatkan partisipasi publik dalam proses penyelenggaran siaran mulai dari tahap perencanaan, pelaksanaan, hingga evaluasi program siaran. <br>
		8.	Meningkatkan kualitas audio dan memperluas jangkauan siaran secara nasional dan internasional dengan mengoptimalkan sumberdaya teknologi yang ada dan mengadaptasi perkembangan teknologi penyiaran serta mengefisienkan pengelolaan operasional maupun pemeliharaan perangkat teknik. <br>
		9.	Mengembangkan organisasi yang dinamis, efektif, dan efisien dengan sistem manajemen sumberdaya (SDM, keuangan, asset, informasi dan operasional) berbasis teknologi informasi dalam rangka mewujudkan tata kelola lembaga yang baik ( good corporate governance ). <br>
		10.	Meningkatkan kualitas siaran luar negeri dengan program siaran yang mencerminkan politik negara dan citra positif bangsa. <br>
		11.	Memberikan pelayanan jasa-jasa yang terkait dengan penggunaan dan pemanfaatan asset negara secara profesional dan akuntabel serta menggali sumber-sumber penerimaan lain untuk mendukung operasional siaran dan meningkatkan kesejahteraan pegawai.

		</p>
	</div>
</body>
</html>